$(document).ready(async function () {
    const slider = $('.slider').slick({
        dots: true,
        infinite: true,
        centerMode: true,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 3,
        arrows: true,
        centerPadding: '50px',

        /*  responsive: [
              {
                  breakpoint: 1024,
                  settings: {
                      slidesToShow: 3,
                      slidesToScroll: 3,
                      infinite: true,
                      dots: true
                  }
              },
              {
                  breakpoint: 600,
                  settings: {
                      slidesToShow: 2,
                      slidesToScroll: 2
                  }
              },
              {
                  breakpoint: 480,
                  settings: {
                      slidesToShow: 1,
                      slidesToScroll: 1
                  }
              }
              // You can unslick at a given breakpoint now by adding:
              // settings: "unslick"
              // instead of a settings object
          ]*/
    });
    const {data: photos} = await axios.get('https://jsonplaceholder.typicode.com/photos?_limit=10')
    console.log(slider)
    for (const photo of photos) {
        const html = $('#slide').html()
            .replace('__url__', photo.url)
            .replace('__title__', photo.title)
        slider.slick('slickAdd', html);
    }
    slider.on('beforeChange', function (event) {
        console.log(slider.slick('slickCurrentSlide'));
    })

    const card = $('.card')
    let myModal = $('#modal')
    let modalContent= $('.modal-content')
    let text = $('.modal-body')
    const closing = $('.close')
    card.each(function () {
        $(this).click(function () {
            myModal.css("display", "block")
            $(this.childNodes[1]).clone().appendTo(modalContent)
            console.log($(this.childNodes[3]).html())
            $(this.childNodes[3]).clone().appendTo(modalContent)
        })
    })

    closing.click(function () {
        myModal.css("display", "none")
        modalContent.empty()
    })
});


